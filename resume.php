<!DOCTYPE html PUBLIC"-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html>

<head>
	<title> Daisy's Resume </title>
	
	<style type = "text/css">
		
	body	{	
			position:relative;
			background-image:url(background.png);
			background-size:100%;
			}
	#L1		{
			position:absolute;
			height:200px;
			width:670px;
			top:-.3%;
			left:100%;
			background-color:#4e4d4c;
			border-top:5px;
			border-right:0px;
			border-bottom:5px;
			border-style:solid;
			border-top-color:#4e4d4c;
			border-bottom-color:#4e4d4c;
			
			
			}
	#name	{
			position:absolute;
			top:3%;
			left:0%;
			padding-top:5px;
			padding-bottom:5px;
			padding-left:165.5px;
			padding-right:165.5px;
			background-color:white;
			border-bottom:30px;
			border-top:30px;
			border-right:0px;
			border-left:0px;
			border-bottom-color:#c6c6c4;
			border-top-color:#c6c6c4;
			border-style:solid;
			background-color:#4e4d4c;
			color:white;
			letter-spacing:1px;
			font-family:angsana new;
			font-size:43px;
			}
	#L2		{
			position:absolute;
			height:1403px;
			width:300px;
			background-color:#c6c6c4;
			left:16.3%;
			top:27%;
			border-top:5px;
			border-left:5px;
			border-right:0px;
			border-bottom:5px;
			border-style:solid;
			border-top-color:#4e4d4c;
			border-bottom-color:#4e4d4c;
			border-left-color:#4e4d4c;
			
			}
	#Photo	{
			position:absolute;
			height:210px;
			top:2%;
			left:14.9%;
			}
	#L2con	{
			position:absolute;
			height:120px;
			width:260px;
			left:6%;
			top:19.5%;
			background-color:#bd9d3b;
			border-left-color:#bd9d3b;
			border-right-color:#bd9d3b;
			border-bottom:10px;
			border-top:10px;
			border-bottom-color:#4e4d4c	 ;
			border-top-color:#4e4d4c;
			border-style:solid;
			}
	#L2num	{
			position:absolute;
			color:black;
			left:32%;
			top:2%;
			font-family:angsana new;
			font-size:28px;
			font-weight:bold;
			}
	#home	{
			position:absolute;
			left:2%;
			top:33%;
			transition: all .5s ease-in-out;
			}
#home:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	#facebook{
			position:absolute;
			left:-7%;
			top:17%;
			transition: all .5s ease-in-out;
			}
#facebook:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	#instagram{
			position:absolute;
			left:1%;
			top:54.5%;
			transition: all .5s ease-in-out;
			}
#instagram:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	#insta {
			position:absolute;
			font-family:angsana new;
			font-size:26px;
			font-weight:bold;
			color:black;
			top:-30%;
			left:120%;
			letter-spacing:1px;
			}
	#fbF {	
			position:absolute;
			font-family:angsana new;
			font-size:25px;
			font-weight:bold;
			color:black;
			top:-17%;
			left:70.5%;
			letter-spacing:1px;
			}
	#gm	{
			position:absolute;
			color:#ede2c0;
			text-decoration:underline;
			font-style:italic;
			left:81%;
			top:-20%;
			font-family:angsana new;
			font-size:25px;
			font-weight:bold;
			}
	#gmail  {
			position:absolute;
			left:-3%;
			top:48%;
			transition: all .5s ease-in-out;
			}
#gmail:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
			
	#phone	{
			position:absolute;
			left:12%;
			top:19%;
			transition: all .5s ease-in-out;
			}
#phone:hover img	{
			transition-delay:.5s;
			transform: scale(1.4) rotate(-290deg);
			}
	#L2C	{
			position:absolute;
			background-color:#4e4d4c;
			font-family:angsana new;
			padding-top:3px;
			padding-bottom:3px;
			padding-left:25px;
			padding-right:25px;
			top:-41%;
			left:15.5%;
			font-weight:bold;
			font-size:23px;
			text-decoration:underline;
			color:white;
			}
	#L2home	{
			position:absolute;
			height:100px;
			width:260px;
			left:6.5%;
			top:32.5%;
			background-color:#bd9d3b;
			border-left-color:#bd9d3b;
			border-right-color:#bd9d3b;
			border-bottom:10px;
			border-top:10px;
			border-bottom-color:#4e4d4c	 ;
			border-top-color:#4e4d4c;
			border-style:solid;
			}
	#L2H	{
			position:absolute;
			background-color:#4e4d4c;
			font-family:angsana new;
			padding-top:3px;
			padding-bottom:3px;
			padding-left:45px;
			padding-right:45px;
			top:-48%;
			left:14%;
			font-weight:bold;
			font-size:23px;
			text-decoration:underline;
			color:white;
			}
	#L2A	{
			position:absolute;
			color:black;
			left:23%;
			top:2%;
			font-family:angsana new;
			font-size:23px;
			font-weight:bold;
			}
	#L2social	{
			position:absolute;
			height:140px;
			width:260px;
			left:7%;
			top:44%;
			background-color:#bd9d3b;
			border-left-color:#bd9d3b;
			border-right-color:#bd9d3b;
			border-bottom:10px;
			border-top:10px;
			border-bottom-color:#4e4d4c	 ;
			border-top-color:#4e4d4c;
			border-style:solid;
			}
	#L2S	{
			position:absolute;
			background-color:#4e4d4c;
			font-family:angsana new;
			padding-top:3px;
			padding-bottom:3px;
			padding-left:72px;
			padding-right:72px;
			top:-36%;
			left:14.5%;
			font-weight:bold;
			font-size:23px;
			text-decoration:underline;
			color:white;
			}
	#L2language{
			position:absolute;
			height:170px;
			width:260px;
			left:7%;
			top:58%;
			background-color:#bd9d3b;
			border-left-color:#bd9d3b;
			border-right-color:#bd9d3b;
			border-bottom:10px;
			border-top:10px;
			border-bottom-color:#4e4d4c	 ;
			border-top-color:#4e4d4c;
			border-style:solid;
			}
	#L2L	{
			position:absolute;
			background-color:#4e4d4c;
			font-family:angsana new;
			padding-top:3px;
			padding-bottom:3px;
			padding-left:59px;
			padding-right:59px;
			top:-27.5%;
			left:15.5%;
			font-weight:bold;
			font-size:23px;
			text-decoration:underline;
			color:white;
			}
	#L2Eng {
			position:absolute;
			font-family:angsana new;
			font-size:28px;
			font-weight:bold;
			color:black;
			top:-39%;
			letter-spacing:1px;
			left:120%;
			}
	#Eng	{
			position:absolute;
			top:11.5%;
			left:16%;
			transition: all .5s ease-in-out;
			}
#Eng:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	#L2Tag {
			position:absolute;
			font-family:angsana new;
			font-size:28px;
			font-weight:bold;
			color:black;
			top:-45%;
			letter-spacing:1px;
			left:115%;
			}
	#Tag	{
			position:absolute;
			top:39%;
			left:26%;
			transition: all .5s ease-in-out;
			}
#Tag:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	#L2Bis {
			position:absolute;
			font-family:angsana new;
			font-size:28px;
			font-weight:bold;
			color:black;
			top:-46.5%;
			letter-spacing:1px;
			left:122%;
			}
	#Bis	{
			position:absolute;
			top:67%;
			left:15.5%;
			transition: all .5s ease-in-out;
			}
#Bis:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	
	#L2Interest{
			position:absolute;
			height:285px;
			width:260px;
			left:7%;
			top:73.7%;
			background-color:#bd9d3b;
			border-left-color:#bd9d3b;
			border-right-color:#bd9d3b;
			border-bottom:10px;
			border-top:10px;
			border-bottom-color:#4e4d4c	 ;
			border-top-color:#4e4d4c;
			border-style:solid;
			}
	#L2I	{
			position:absolute;
			background-color:#4e4d4c;
			font-family:angsana new;
			padding-top:3px;
			padding-bottom:3px;
			padding-left:68px;
			padding-right:68px;
			top:-16.5%;
			left:15%;
			font-weight:bold;
			font-size:23px;
			text-decoration:underline;
			color:white;
			}
	#L21st {
			position:absolute;
			font-family:angsana new;
			font-size:28px;
			font-weight:bold;
			color:black;
			top:-23%;
			left:140%;
			letter-spacing:1px;
			}
	#travel	{
			position:absolute;
			top:8%;
			left:8%;
			transition: all .5s ease-in-out;
			}
#travel:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	#bike	{
			position:absolute;
			top:28%;
			left:8%;
			transition: all .5s ease-in-out;
			}
#bike:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	#cook	{
			position:absolute;
			top:44%;
			left:10%;
			transition: all .5s ease-in-out;
			}
#cook:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	#baking	{
			position:absolute;
			top:62%;
			left:8%;
			transition: all .5s ease-in-out;
			}
#baking:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	#swimming	{
			position:absolute;
			top:79%;
			left:7.5%;
			transition: all .5s ease-in-out;
			}
#swimming:hover img	{
			transition-delay:.15s;
			transform: scale(1.2) rotate(350deg);
			}
	#L22nd {
			position:absolute;
			font-family:angsana new;
			font-size:28px;
			font-weight:bold;
			color:black;
			top:-35%;
			left:135%;
			}
	#L23rd {
			position:absolute;
			font-family:angsana new;
			font-size:28px;
			font-weight:bold;
			color:black;
			top:-31%;
			left:267%;
			}
	#L24th {
			position:absolute;
			font-family:angsana new;
			font-size:28px;
			font-weight:bold;
			color:black;
			top:-43%;
			left:200%;
			}
	#L25th {
			position:absolute;
			font-family:angsana new;
			font-size:28px;
			font-weight:bold;
			color:black;
			top:-39%;
			left:150%;
			}
	#L3		{
			position:absolute;
			height:280px;
			width:670px;
			top:16.2%;
			left:100%;
			background-color:#4e4d4c;
			border-top:5px;
			border-left:0px;
			border-right:0px;
			border-bottom:5px;
			border-style:solid;
			border-top-color:#c6c6c4;
			border-bottom-color:#c6c6c4;
			
			}
	#P		{
			position:absolute;
			background-color:#bd9d3b;
			font-family:angsana new;
			padding-top:10px;
			padding-bottom:10px;
			padding-left:80px;
			padding-right:80px;
			top:-13%;
			left:25%;
			font-weight:bold;
			font-size:20px;
			color:white;
			
			}
	#L3info	{
			position:absolute;
			font-family:angsana new;
			font-weight:bold;
			color:#ede2c0;
			top:12%;
			left:8%;
			font-size:23px;
			}
	#L3info2{
			position:absolute;
			font-family:angsana new;
			font-weight:bold;
			color:#ede2c0;
			top:12%;
			left:50%;
			font-size:23px;
			}
	
	#L4		{
			position:absolute;
			height:620px;
			width:670px;
			top:38.7%;
			left:100%;
			background-color:#4e4d4c;
			border-top:5px;
			border-left:0px;
			border-right:0px;
			border-bottom:5px;
			border-style:solid;
			border-top-color:#c6c6c4;
			border-bottom-color:#c6c6c4;
			
			}
	#E		{
			position:absolute;
			background-color:#bd9d3b;
			font-family:angsana new;
			padding-top:10px;
			padding-bottom:10px;
			padding-left:73px;
			padding-right:73px;
			top:-6%;
			left:25%;
			font-weight:bold;
			font-size:20px;
			color:white;
			
			}	
	#L4educ {
			position:absolute;
			font-family:angsana new;
			font-weight:bold;
			color:#ede2c0;
			top:2%;
			left:5%;
			font-size:23px;
			}
	#L4educ2 {
			position:absolute;
			font-family:angsana new;
			font-weight:bold;
			color:#ede2c0;
			top:5.8%;
			left:67%;
			font-size:23px;
			}
	#L5		{
			position:absolute;
			height:200px;
			width:670px;
			top:85.4%;
			left:100%;
			background-color:#4e4d4c;
			border-top:5px;
			border-left:0px;
			border-right:0px;
			border-bottom:5px;
			border-style:solid;
			border-top-color:#c6c6c4;
			border-bottom-color:#c6c6c4;
			
			}
	#R		{
			position:absolute;
			background-color:#bd9d3b;
			font-family:angsana new;
			padding-top:10px;
			padding-bottom:10px;
			padding-left:125px;
			padding-right:125px;
			top:-19%;
			left:26%;
			font-weight:bold;
			font-size:20px;
			color:white;
			
			}
	#may	{
			position:absolute;
			height:300px;
			top:-18%;
			left:-11%;
			}
	#ris	{
			position:absolute;
			height:220px;
			top:5.5%;
			left:43%;
			}
	#mayinfo{
			position:absolute;
			top:22%;
			left:23%;
			font-family:angsana new;
			font-size:20px;
			color:#ede2c0;
			font-weight:bold;
			}
	#risinfo{
			position:absolute;
			top:22.5%;
			left:70.8%;
			font-family:angsana new;
			font-size:20px;
			color:#ede2c0;
			font-weight:bold;
			}
	#em 	{
			position:absolute;
			color:orange;
			font-family:angsana new;
			font-size:22px;
			text-decoration:underline;
			left:20.5%;
			top:60%;
			}
	#er 	{
			position:absolute;
			color:orange;
			font-family:angsana new;
			font-size:22px;
			text-decoration:underline;
			left:69.9%;
			top:60%;
			}
	
	</style>
</head>

<body>

	<div id = "L2">
	
	<div id = "L1">
		<div>
			<h1 id = "name" > DAISYREE VARIACION </h1>
		</div>
	</div>
		<img src = "pic.png" id = "Photo" />
		
		<div id = "L2con">
			<p id = "L2C">Contact Information</p>
			<p id = "L2num" >09506486378</p>
			<div id ="phone">
				<img src = "phone.png" height="57px"/>
			</div>
			<div id ="gmail">
				<img src = "gmail.png" height="68px" title = "gmail.com"/> 
				<p id ="gm" >daisyreeviacion@gmail.com</p>
			
			</div>
			
		</div>
		
		<div id = "L2home">
			<p id = "L2H">Home Address</p>
			<p id = "L2A" >#355 McArthur Highway,
			<br />Matina, Davao City</p>
			<div id ="home">
				<a href = "https://www.google.com/maps/@7.0572086,125.5721712,3a,45y,240.65h,89.19t/data=!3m6!1e1!3m4!1sQsMKj50tccAPxKnCBwC51Q!2e0!7i13312!8i6656">
				<img src = "home.png" height="55px" title = "googlemap.com"/></a>
			</div>
		</div>
		
		<div id = "L2social">
			<p id = "L2S">Social</p>
			<div id ="facebook">
				<a href = "https://www.facebook.com/daisy.variacion"
				><img src = "facebook.png" height="50px" title = "facebook.com" /></a>
				<p id = "fbF" >@daisyreeolañavariacion</p>
				
			</div>
			<div id ="instagram">
				<a href = "https://www.instagram.com/eerysiad22/">
				<img src = "instagram.png" height="53px" title = "instagram.com"/></a>
				<p id = "insta">@eerysiad22</p>
			</div>
			
		</div>
		
		<div id = "L2language">
			<p id = "L2L">Language</p>
			
			<div id ="Eng">
				<img src = "eng.png" height="49px"/>
				<p id = "L2Eng">English</p>
			</div>
			<div id ="Tag">
				<img src = "tag.png" height="51px"/>				
				<p id = "L2Tag" >Tagalog</p>
			</div>
			
			<div id ="Bis">
				<img src = "bis.png" height="45px"/>	
				<p id = "L2Bis" >Bisaya</p>
			</div>
		</div>
		<div id = "L3">
		<div>
			<h3 id = "P" >- PERSONAL INFORMATION -<h3>
		</div>
		
		<div id = "L3info">
			<ul>
				<li>Birthdate: July 4 1996</li>
				<li>Age: 22 yrs. old</li>
				<li>Blood Type: O positive</li>
				<li>Civil Status: Single</li>
				<li>Father's Name: Desiderio Variacion</li>
				<li>Mother's Name: Teresita Variacion</li>
			</ul>
		</div>
		
		<div id = "L3info2">
			<ul>
				<li>Birthplace: Davao City</li>
				<li>Gender: Female</li>
				<li>Religion: Roman Catholic</li>
				<li>Nationality: Filipino</li>
				<li>Occupation: Carpenter</li>
				<li>Occupation: Domestic Helper</li>
			</ul>
		</div>
	</div>
	
	<div id = "L4">
		<div>
			<h3 id = "E" >- EDUCATIONAL ATTAINMENT -<h3>
		</div>
		<div >
			<ul id = "L4educ">
				<p><span>PRIMARY</span></P>
				<li>Matina Aplaya Elementary School</li>
				<p><span>SECONDARY</span></P>
				<li>Daniel R. Aguinaldo National High School</li><br />
				<li>Eulalio U. Pabillore National High School</li>
				<p><span>TERTIARY</span></p>
				<li>Camiguin Polytechnic State College</li>
				<p><em>Bachelor of Elementary Education</em></p>
				<li>University of Southeastern Philippines</li>
				<p><em>Bachelor of Information Technology</em></p>                       
			</ul>
		</div>
		<div id = "L4educ2">
				<p>SCHOOL YEAR</p>
				<p>2003 - 2009</p><br />
				<p>2009 - 2011</p>
				<p>2012 - 2014</p><br />
				<p>2014 - 2015</p>
				<p><em>Undergraduate</em></p>
				<p>2016 - PRESENT</p>
				<p><em>Undergraduate</em></p>
		
		</div>
		
	</div>
	<div id = "L5">
		<div>
			<h3 id = "R" >- REFERRENCE -<h3>
		</div>
		
			<img src = "mayet.png" id = "may"/>
		
			<p id = "mayinfo" >MAYETTE LANTACA
			<br /> Civil Engineer
			<br /> 09107279266 
			<br /><a href = "http://gmail.com"></a>
			</p>
			<p id = "em">mayettelantaca@gmail.com</p>
		
			<img src = "ris.png" id = "ris" />
	
			<p id = "risinfo" >IRIS JANE BALLENA
			<br /> Call Center Agent
			<br /> 09103957462
			<br /><a href = "http://gmail.com"></a>
			<p id = "er">ijaneling@gmail.com</p></p>
	</div>
		
		<div id = "L2Interest">
			
			<p id = "L2I">Interest</p>
			<div id = "travel">
			<p id = "L21st">Travelling</p>
			<img src = "travel.png" height = "50px" />
			</div>
			<div id = "bike">
			<p id = "L22nd" >Biking</p>
			<img src = "bike.png" height = "50px" />
			</div>
			<div id = "cook">
			<p id = "L23rd" >Cooking</p>
			<img src = "cook.png" height = "50px" />
			</div>
			<div id = "baking">
			<p id = "L24th" >Baking</p>
			<img src = "baking.png" height = "50px" />
			</div>
			<div id = "swimming">
			<p id = "L25th" >Swimming</p>
			<img src = "swimming.png" height = "50px" />
			</div>
		</div>
		
	</div>
	

</body>


<html>